# README #

### General info ###

* This app is a test harness template for testing REEF services that provides API and UI layer capabilities.
* It's loosely based on the [aaa-automation](https://bitbucket.org/parkjockey/aaa-automation) project


* Significant changes include:
  * horizontal app structure instead of the previous vertical "by-test-layer" approach
  * non-thread-safe classes/methods have been made thread-safe
  * addition of Junit5 infrastructure for writing plain Junit5 tests with same useful mechanisms that Cucumber brought
  * sensitive information (e.g. credentials) is not stored in the project
  * Cucumber is made to use Junit5 engine instead of Junit4's
  * Database-related capabilities are removed
  * Allure reporting is removed (to be added at a later time)
* Version 1.0.0

### How do I get set up? ###

* Summary of set up

### Configuration ####
* Project requires Java version 14 or higher
* Make sure to have "Run tests using" option set to "Gradle" for Intellij and the equivalent setting in other IDE's

### Dependencies ####

### Running tests ###
There are several optional and overridable project/system properties that are used (set in the build.gradle file):
  * "testrail" - boolean for controlling post-run Testrail report generation using their API (default: "false")
  * "parallel" - boolean for controlling parallel test execution (default: "true")
  * "target_env" - for specifying the REEF environment we want our tests to run against (default: "qa", possible values: "qa", "dev", "stage", "prod")
  * "tags" - for running tests with arbitrary tags (default: "regression")


Command examples:
  * <code>./gradlew clean {{gradle-task-name}}</code> -> run whatever tags are defined to be included for that gradle task
  * <code>./gradlew clean test</code> -> run all tests except cucumber ones 
  * <code>./gradlew clean test -Ptags=wip</code> -> run only the tests marked with either @WIP or Tag("wip") junit annotation
  * <code>./gradlew clean cucumber</code> -> run the CucumberSuite.class (and thus whatever .feature files are in the appropriate directory relative to mentioned class )
  * <code>./gradlew clean {{gradle-task-name}} -Ptarget_env=dev -Ptestrail=false -Pparallel=true</code> -> an example exercising all possible command line arguments when using a custom gradle task
  * <code>./gradlew clean test -Ptags={{some-tag}} -Ptarget_env=dev -Ptestrail=false -Pparallel=true</code> -> an example exercising all possible command line arguments when using test gradle task

### Contribution guidelines ###
* When writing step definitions, prefer "annotation style" over "lambda style" ones, given the documented intent of the Cucumber team to deprecate cucumber-java8 module at some point([source](https://github.com/cucumber/cucumber-jvm/issues/2174))
* Make sure to avoid state sharing between tests to allow for parallel execution

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact