package com.reef.reefproject.webdriver;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.ArrayList;
import java.util.List;

public final class ThreadLocalDriver {

    // prevent instantiation
    private ThreadLocalDriver() {
        throw new AssertionError("Instantiation attempted from within class");
    }

    private static final ThreadLocal<RemoteWebDriver> drivers = new ThreadLocal<>();

    // quit the drivers and browsers at the end only
    private static final List<RemoteWebDriver> storedDrivers = new ArrayList<>();

    static {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> storedDrivers.forEach(WebDriver::quit)));
    }

    public static WebDriver getDriver() {
        return drivers.get();
    }

    public static void addDriver(RemoteWebDriver driver) {
        storedDrivers.add(driver);
        drivers.set(driver);
    }

    public static void removeDriver() {
        storedDrivers.remove(drivers.get());
        drivers.remove();
    }

    public static String getInfo() {
        Capabilities cap = ((RemoteWebDriver) getDriver()).getCapabilities();
        String browserName = cap.getBrowserName();
        String platform = cap.getPlatformName().toString();
        String version = cap.getBrowserVersion();
        return String.format("browser: %s v: %s platform: %s", browserName, version, platform);
    }
}