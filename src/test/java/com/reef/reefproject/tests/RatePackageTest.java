package com.reef.reefproject.tests;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.reef.reefproject.helpers.assertions.RatePackageAssertion;
import com.reef.reefproject.httpclient.graphql.ReefGraphqlClient;
import com.reef.reefproject.httpclient.graphql.TokenAuthClient;
import com.reef.reefproject.junit.annotations.Smoke;
import com.reef.reefproject.model.ReefGraphqlRequest;
import com.reef.reefproject.model.ratepackage.request.*;
import com.reef.reefproject.model.ratepackage.response.CopyRatePackageResponse;
import com.reef.reefproject.model.ratepackage.response.GetRatePackageResponse;
import com.reef.reefproject.model.ratepackage.response.RatePackageReefCloud;
import com.reef.reefproject.model.ratepackage.response.UpdateRatePackageResponse;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.apache.hc.core5.http.HttpStatus;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
class RatePackageTest {

    private final ObjectMapper configuredObjectMapper;

    private RatePackageTest(ObjectMapper configuredObjectMapper) {
        this.configuredObjectMapper = configuredObjectMapper;
    }

    @Smoke
    @Test
    void creating_default_valid_rate_package_should_succeed() throws JsonProcessingException {
        new RatePackageCreator(configuredObjectMapper).createAndAssertRandomValidRatePackage();
    }

    @Test
    void copying_a_rate_package_with_provided_valid_changes_should_succeed() throws JsonProcessingException {
        // ARRANGE
        // create the rate package to be copied
        RatePackageCreator ratePackageCreator = new RatePackageCreator(configuredObjectMapper);
        RatePackageReefCloud createdRatePackage = ratePackageCreator.createAndAssertRandomValidRatePackage();
        log.info("Created rate package: " + createdRatePackage.toString());

        // construct arguments object with default random valid values
        RatePackageInput ratePackageInput = RatePackageInput.builder()
                .longTerm(true)
                .build();
        log.info("Desired rate package update: " + ratePackageInput.toString());

        // construct the request body
        CopyRatePackageRequest copyRatePackageBody = new CopyRatePackageRequest(createdRatePackage.getId(), ratePackageInput);

        // construct complete request
        ReefGraphqlRequest copyRatePackage = new CopyRatePackage(copyRatePackageBody);
        ReefGraphqlClient authorizedClient = new TokenAuthClient();

        // ACT
        Response response = authorizedClient.send(copyRatePackage);
        CopyRatePackageResponse copyRatePackageResponse = configuredObjectMapper.readValue(response.getBody().asString(), CopyRatePackageResponse.class);

        // ASSERT
        assertEquals(HttpStatus.SC_OK, response.getStatusCode());
        assertNotNull(copyRatePackageResponse.getData(), "Response data object should be populated");
        RatePackageReefCloud returnedRatePackage = copyRatePackageResponse.getData().getCopyRatePackage();

        RatePackageAssertion.assertThat(returnedRatePackage)
                .hasMatchingFieldValuesAs(ratePackageInput)
                .hasEmptyRatePlansAndFees()
                .andAnId();
    }

    @Test
    void fetching_an_existing_rate_package_should_succeed() throws JsonProcessingException{
        // ARRANGE
        // create the rate package to be fetched
        RatePackageCreator ratePackageCreator = new RatePackageCreator(configuredObjectMapper);
        RatePackageReefCloud createdRatePackage = ratePackageCreator.createAndAssertRandomValidRatePackage();

        // construct the request body
        GetRatePackageRequest getRatePackageBody = new GetRatePackageRequest(createdRatePackage.getId());

        // construct complete request
        ReefGraphqlRequest getRatePackage = new GetRatePackage(getRatePackageBody);
        ReefGraphqlClient authorizedClient = new TokenAuthClient();

        // ACT
        Response response = authorizedClient.send(getRatePackage);
        GetRatePackageResponse getRatePackageResponse = configuredObjectMapper.readValue(response.getBody().asString(), GetRatePackageResponse.class);
        assertNotNull(getRatePackageResponse.getData(), "Response data object should be populated");
        RatePackageReefCloud updatedRatePackage = getRatePackageResponse.getData().getGetRatePackage();

        // ASSERT
        assertEquals(HttpStatus.SC_OK, response.getStatusCode());
        RatePackageAssertion.assertThat(updatedRatePackage).hasMatchingFieldValuesAs(createdRatePackage);
    }

    @Test
    void editing_a_rate_package_with_provided_valid_changes_should_succeed() throws JsonProcessingException {
        // ARRANGE
        // create the rate package to be edited
        RatePackageCreator ratePackageCreator = new RatePackageCreator(configuredObjectMapper);
        RatePackageReefCloud createdRatePackage = ratePackageCreator.createAndAssertRandomValidRatePackage();
        log.info("Created rate package: " + createdRatePackage.toString());

        // construct arguments object with different valid values
        RatePackageInput ratePackageInput = RatePackageInput.builder()
                .currency("CAD")
                .mapPrice("0.99")
                .shared(true)
                .bestRate(true)
                .longTerm(true)
                .build();
        log.info("Desired rate package update: " + ratePackageInput.toString());

        // construct the request body
        UpdateRatePackageRequest updateRatePackageBody = new UpdateRatePackageRequest(createdRatePackage.getId(), ratePackageInput);

        // construct complete request
        ReefGraphqlRequest updateRatePackage = new UpdateRatePackage(updateRatePackageBody);
        ReefGraphqlClient authorizedClient = new TokenAuthClient();

        // ACT
        Response response = authorizedClient.send(updateRatePackage);

        UpdateRatePackageResponse updateRatePackageResponse = configuredObjectMapper.readValue(response.getBody().asString(), UpdateRatePackageResponse.class);
        assertNotNull(updateRatePackageResponse.getData(), "Response data object should be populated");
        RatePackageReefCloud updatedRatePackage = updateRatePackageResponse.getData().getUpdateRatePackage();

        // ASSERT
        assertEquals(HttpStatus.SC_OK, response.getStatusCode());
        RatePackageAssertion.assertThat(updatedRatePackage)
                .hasMatchingFieldValuesAs(ratePackageInput)
                .hasEmptyRatePlansAndFees()
                .andAnId();
    }
}