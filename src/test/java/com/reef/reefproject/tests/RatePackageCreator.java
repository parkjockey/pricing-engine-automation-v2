package com.reef.reefproject.tests;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.reef.reefproject.helpers.assertions.RatePackageAssertion;
import com.reef.reefproject.httpclient.graphql.ReefGraphqlClient;
import com.reef.reefproject.httpclient.graphql.TokenAuthClient;
import com.reef.reefproject.model.ReefGraphqlRequest;
import com.reef.reefproject.model.ratepackage.request.CreateRatePackage;
import com.reef.reefproject.model.ratepackage.request.CreateRatePackageRequest;
import com.reef.reefproject.model.ratepackage.request.RatePackageInput;
import com.reef.reefproject.model.ratepackage.response.CreateRatePackageResponse;
import com.reef.reefproject.model.ratepackage.response.RatePackageReefCloud;
import io.restassured.response.Response;
import org.apache.hc.core5.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class RatePackageCreator {

    private final ObjectMapper configuredObjectMapper;

    public RatePackageCreator(ObjectMapper configuredObjectMapper) {
        this.configuredObjectMapper = configuredObjectMapper;
    }

    public RatePackageReefCloud createAndAssertRandomValidRatePackage() throws JsonProcessingException {
        // ARRANGE
        // construct arguments object with default random valid values
        RatePackageInput ratePackageInput = new RatePackageInput();

        // construct the request body
        CreateRatePackageRequest createRatePackageBody = new CreateRatePackageRequest(ratePackageInput);

        // construct complete request
        ReefGraphqlRequest createRatePackage = new CreateRatePackage(createRatePackageBody);
        ReefGraphqlClient authorizedClient = new TokenAuthClient();

        // ACT
        Response response = authorizedClient.send(createRatePackage);

        // deserialize returned response
        CreateRatePackageResponse createRatePackageResponse = configuredObjectMapper.readValue(response.getBody().asString(), CreateRatePackageResponse.class);
        assertNotNull(createRatePackageResponse.getData(), "Response data object should be populated");
        RatePackageReefCloud returnedRatePackage = createRatePackageResponse.getData().getCreateRatePackage();

        // ASSERT
        assertEquals(HttpStatus.SC_OK, response.getStatusCode());
        RatePackageAssertion.assertThat(returnedRatePackage)
                .hasMatchingFieldValuesAs(ratePackageInput)
                .hasEmptyRatePlansAndFees()
                .andAnId();

        return returnedRatePackage;
    }
}
