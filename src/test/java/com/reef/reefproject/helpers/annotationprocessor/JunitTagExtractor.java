package com.reef.reefproject.helpers.annotationprocessor;

import com.reef.reefproject.junit.annotations.TestrailCaseId;
import org.junit.jupiter.api.extension.ExtensionContext;

public class JunitTagExtractor implements TagExtractor {

    private final ExtensionContext context;

    public JunitTagExtractor(ExtensionContext context) {
        this.context = context;
    }

    @Override
    public int extractByTagName(String tag) {
        int testCaseId = 0;

        for (String tagName : context.getTags()) {
            if (tagName.contains(tag)) {
                testCaseId = context.getElement().orElseThrow().getAnnotation(TestrailCaseId.class).value();
            }
        }
        return testCaseId;
    }
}