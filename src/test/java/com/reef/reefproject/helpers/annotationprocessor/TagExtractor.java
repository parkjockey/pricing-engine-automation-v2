package com.reef.reefproject.helpers.annotationprocessor;

public interface TagExtractor {

    int extractByTagName(String tag);
}
