package com.reef.reefproject.helpers.generators;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public final class RandomGenerator {

    // prevent instantiation
    private RandomGenerator() {
        throw new AssertionError("Instantiation attempted from within class");
    }

    public static Integer getRandomNumber(int min, int max) {
        ThreadLocalRandom threadLocalRandom = ThreadLocalRandom.current();
        return min + threadLocalRandom.nextInt(max);
    }

    public static Integer getRandomNumber() {
        int max = 1000000;
        int min = 1;
        ThreadLocalRandom threadLocalRandom = ThreadLocalRandom.current();
        return min + threadLocalRandom.nextInt(max);
    }

    public static String getRandomStringOfChars(int desiredLength) {
        return getRandomChars(desiredLength, true);
    }

    public static String getRandomAlphanumericString(int desiredLength) {
        return getRandomChars(desiredLength, false);
    }

    private static String getRandomChars(int desiredLength, boolean specialCharsAllowed) {
        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        if (specialCharsAllowed) {
            chars += "$&@?<>~!%#";
        }
        StringBuilder strBuilder = new StringBuilder();
        Random rnd = new Random();
        while (strBuilder.length() < desiredLength) {
            int index = (int) (rnd.nextFloat() * chars.length());
            strBuilder.append(chars.charAt(index));
        }
        return strBuilder.toString();
    }
}