package com.reef.reefproject.helpers.generators;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public final class DatetimeGenerator {

    // prevent instantiation
    private DatetimeGenerator() {
        throw new AssertionError("Instantiation attempted from within class");
    }

    public static String getDatetimeOfPattern(String pattern) {
        LocalDateTime dateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return dateTime.format(formatter);
    }

    public static String getDatetimeOfPattern(LocalDateTime localDateTime, String pattern) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return localDateTime.format(formatter);
    }
}