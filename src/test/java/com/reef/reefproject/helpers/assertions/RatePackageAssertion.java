package com.reef.reefproject.helpers.assertions;

import com.reef.reefproject.model.ratepackage.request.RatePackageInput;
import com.reef.reefproject.model.ratepackage.response.RatePackageReefCloud;
import org.assertj.core.api.AbstractAssert;
import org.junit.jupiter.api.Assertions;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RatePackageAssertion extends AbstractAssert<RatePackageAssertion, RatePackageReefCloud> {

    protected RatePackageAssertion(RatePackageReefCloud actual) {
        super(actual, RatePackageAssertion.class);
    }

    public static RatePackageAssertion assertThat(RatePackageReefCloud actual) {
        return new RatePackageAssertion(actual);
    }

    public RatePackageAssertion hasMatchingFieldValuesAs(RatePackageInput expectedRatePackage) {
        isNotNull();

        Assertions.assertAll("Rate package should be created as intended",
                () -> assertEquals(expectedRatePackage.getRatePackageName(), actual.getName()),
                () -> assertEquals(expectedRatePackage.getServiceCode(), actual.getServiceCode()),
                () -> assertEquals(expectedRatePackage.getMarketName(), actual.getMarketName()),
                () -> assertEquals(expectedRatePackage.getCurrency(), actual.getCurrency()),
                () -> assertEquals(expectedRatePackage.getMapPrice(), actual.getMapPrice()),
                () -> assertEquals(expectedRatePackage.getTaxInclusive(), actual.getTaxInclusive()),
                () -> assertEquals(expectedRatePackage.getShared(), actual.getShared()),
                () -> assertEquals(expectedRatePackage.getBestRate(), actual.getBestRate()),
                () -> assertEquals(expectedRatePackage.getLongTerm(), actual.getLongTerm())
        );
        return this;
    }

    public RatePackageAssertion hasEmptyRatePlansAndFees() {
        Assertions.assertAll("Rate package should be created without rate plans and fees",
                () -> assertTrue(actual.getRatePlans().isEmpty(), "Rate packages should be created without any rate plans"),
                () -> assertTrue(actual.getFees().isEmpty(), "Rate packages should be created without any fees")
        );
        return this;
    }

    public void andAnId() {
        Assertions.assertAll("Rate package should have a UUID generated for it",
            () -> assertNotNull(actual.getId(), "An id should be generated"),
            () -> assertNotEquals("", actual.getId(), "The id should not be an empty string")
        );
    }

    public void hasMatchingFieldValuesAs(RatePackageReefCloud expectedRatePackage) {
        Assertions.assertAll("Fetched rate package should be identical to previously created rate package",
                () -> assertEquals(expectedRatePackage.getId(), actual.getId()),
                () -> assertEquals(expectedRatePackage.getName(), actual.getName()),
                () -> assertEquals(expectedRatePackage.getServiceCode(), actual.getServiceCode()),
                () -> assertEquals(expectedRatePackage.getMarketName(), actual.getMarketName()),
                () -> assertEquals(expectedRatePackage.getCurrency(), actual.getCurrency()),
                () -> assertEquals(expectedRatePackage.getMapPrice(), actual.getMapPrice()),
                () -> assertEquals(expectedRatePackage.getTaxInclusive(), actual.getTaxInclusive()),
                () -> assertEquals(expectedRatePackage.getRatePlans(), actual.getRatePlans()),
                () -> assertEquals(expectedRatePackage.getFees(), actual.getFees()),
                () -> assertEquals(expectedRatePackage.getShared(), actual.getShared()),
                () -> assertEquals(expectedRatePackage.getBestRate(), actual.getBestRate()),
                () -> assertEquals(expectedRatePackage.getLongTerm(), actual.getLongTerm())
        );
    }
}