package com.reef.reefproject.helpers.parsers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.*;

public final class GraphqlParser {

    // prevent instantiation
    private GraphqlParser() {
        throw new AssertionError("Instantiation attempted from within class");
    }

    public static String getMappedGraphqlString(File file, String variables) throws IOException {
        String graphqlFileContent = convertInputStreamToString(new FileInputStream(file));
        return mapGraphqlRequestBody(graphqlFileContent, variables);
    }

    private static String mapGraphqlRequestBody(String graphql, String variables) throws JsonProcessingException {
        ObjectMapper oMapper = new ObjectMapper();
        ObjectNode oNode = oMapper.createObjectNode();
        oNode.put("query", graphql);
        oNode.put("variables", variables);
        return oMapper.writeValueAsString(oNode);
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        StringBuilder sb = new StringBuilder();
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        try (BufferedReader bufferedReader = new BufferedReader(inputStreamReader)) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line).append("\n");
            }
        }
        return sb.toString();
    }
}