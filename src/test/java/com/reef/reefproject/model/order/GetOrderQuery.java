package com.reef.reefproject.model.order;

import com.google.gson.Gson;
import com.reef.reefproject.helpers.parsers.GraphqlParser;
import com.reef.reefproject.model.ReefGraphqlRequest;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;

@Slf4j
public class GetOrderQuery extends ReefGraphqlRequest {

    private final @Getter GetOrderArguments getOrderArguments;
    private final Gson gson = new Gson();
    private static final File REQUEST_MODEL_FILE = new File("src/test/resources/graphqlschema/getOrder.graphql");

    public GetOrderQuery(GetOrderArguments getOrderArguments) {
        this.getOrderArguments = getOrderArguments;
    }

    @Override
    public String toString() {
        String parsedGraphqlPayload = "";
        try {
            parsedGraphqlPayload = GraphqlParser.getMappedGraphqlString(REQUEST_MODEL_FILE, gson.toJson(getOrderArguments));
        } catch (IOException ex) {
            log.debug(ex.getMessage());
        }
        return parsedGraphqlPayload;
    }
}
