package com.reef.reefproject.model.ratesummary;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class RateSummaries {
    private String activeTime;
    private String amount;
}
