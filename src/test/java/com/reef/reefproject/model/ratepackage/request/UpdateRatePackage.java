package com.reef.reefproject.model.ratepackage.request;

import com.reef.reefproject.model.ReefGraphqlRequest;
import lombok.Getter;

import java.io.File;

public class UpdateRatePackage extends ReefGraphqlRequest {

    @Getter private final UpdateRatePackageRequest updateRatePackageRequest;

    public UpdateRatePackage(UpdateRatePackageRequest updateRatePackageRequest) {
        this.updateRatePackageRequest = updateRatePackageRequest;
    }

    @Override
    public String toString() {
        String graphqlFilePath = GRAPHQL_SCHEMA_FILE_PATH + "updateRatePackage.graphql";
        File graphqlRequestModelFile = new File(graphqlFilePath);
        return super.stringify(graphqlRequestModelFile, updateRatePackageRequest);
    }
}