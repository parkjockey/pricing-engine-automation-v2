package com.reef.reefproject.model.ratepackage.request;

import com.google.gson.annotations.SerializedName;
import com.reef.reefproject.model.ReefGraphqlArgument;
import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
public class UpdateRatePackageRequest implements ReefGraphqlArgument {

    @SerializedName("ratePackageId") private String ratePackageId;
    @SerializedName("ratePackageInput") private RatePackageInput ratePackageInput;
}