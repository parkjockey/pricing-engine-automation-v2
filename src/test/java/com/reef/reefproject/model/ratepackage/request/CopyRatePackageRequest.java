package com.reef.reefproject.model.ratepackage.request;

import com.google.gson.annotations.SerializedName;
import com.reef.reefproject.model.ReefGraphqlArgument;
import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class CopyRatePackageRequest implements ReefGraphqlArgument {

    @SerializedName("ratePackageId") private String ratePackageId;
    @SerializedName("ratePackageInput") private RatePackageInput ratePackageInput;
}