package com.reef.reefproject.model.ratepackage.request;

import com.google.gson.annotations.SerializedName;
import com.reef.reefproject.model.ReefGraphqlArgument;
import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
public class GetRatePackageSummaryRequest implements ReefGraphqlArgument {

    @SerializedName("ratePackageSummaryInput")
    private RatePackageSummaryInput ratePackageSummaryInput;

    @Getter
    @Setter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @ToString
    public static class RatePackageSummaryInput{

        @SerializedName("ratePackageId") private String ratePackageId;
    }
}