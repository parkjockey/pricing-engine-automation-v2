package com.reef.reefproject.model.ratepackage.request;

import com.reef.reefproject.model.ReefGraphqlRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.File;

@Slf4j
public class GetRatePackage extends ReefGraphqlRequest {

    @Getter @Setter private GetRatePackageRequest getRatePackageRequest;

    public GetRatePackage(GetRatePackageRequest getRatePackageRequest) {
        this.getRatePackageRequest = getRatePackageRequest;
    }

    @Override
    public String toString() {
        String graphqlFilePath = GRAPHQL_SCHEMA_FILE_PATH + "getRatePackage.graphql";
        File graphqlRequestModelFile = new File(graphqlFilePath);
        return super.stringify(graphqlRequestModelFile, getRatePackageRequest);
    }
}