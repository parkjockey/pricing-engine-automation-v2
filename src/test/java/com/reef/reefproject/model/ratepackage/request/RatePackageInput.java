package com.reef.reefproject.model.ratepackage.request;

import com.google.gson.annotations.SerializedName;
import com.reef.reefproject.helpers.generators.RandomGenerator;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RatePackageInput {

    @SerializedName("serviceCode")
    @Builder.Default
    private String serviceCode = "serviceCode" + RandomGenerator.getRandomNumber();

    @SerializedName("marketName")
    @Builder.Default
    private String marketName = "marketName" + RandomGenerator.getRandomNumber();

    @SerializedName("ratePackageName")
    @Builder.Default
    private String ratePackageName = "ratePackageName" + RandomGenerator.getRandomNumber();

    @SerializedName("currency")
    @Builder.Default
    private String currency = "USD";

    @SerializedName("mapPrice")
    @Builder.Default
    private String mapPrice = "5";

    @SerializedName("taxInclusive")
    @Builder.Default
    private Boolean taxInclusive = false;

    @SerializedName("shared")
    @Builder.Default
    private Boolean shared = false;

    @SerializedName("bestRate")
    @Builder.Default
    private Boolean bestRate = false;

    @SerializedName("realEstateId")
    @Builder.Default
    private String realEstateId = "realEstateId" + RandomGenerator.getRandomNumber();

    @SerializedName("longTerm")
    @Builder.Default
    private Boolean longTerm = false;
}