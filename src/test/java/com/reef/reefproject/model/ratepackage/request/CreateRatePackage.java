package com.reef.reefproject.model.ratepackage.request;

import com.reef.reefproject.model.ReefGraphqlRequest;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.io.File;

@Slf4j
public class CreateRatePackage extends ReefGraphqlRequest {

    @Getter private final CreateRatePackageRequest createRatePackageRequest;

    public CreateRatePackage(CreateRatePackageRequest createRatePackageRequest) {
        this.createRatePackageRequest = createRatePackageRequest;
    }

    @Override
    public String toString() {
        String graphqlFilePath = GRAPHQL_SCHEMA_FILE_PATH + "createRatePackage.graphql";
        File graphqlRequestModelFile = new File(graphqlFilePath);
        return super.stringify(graphqlRequestModelFile, createRatePackageRequest);
    }
}