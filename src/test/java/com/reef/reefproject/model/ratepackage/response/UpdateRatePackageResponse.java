package com.reef.reefproject.model.ratepackage.response;

import com.google.gson.annotations.SerializedName;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
public class UpdateRatePackageResponse {

    @SerializedName("data")
    private Data data;

    @Getter
    public static class Data {

        @SerializedName("updateRatePackage")
        private RatePackageReefCloud updateRatePackage;
    }
}