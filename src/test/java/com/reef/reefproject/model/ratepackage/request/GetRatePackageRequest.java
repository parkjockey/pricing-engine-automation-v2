package com.reef.reefproject.model.ratepackage.request;

import com.google.gson.annotations.SerializedName;
import com.reef.reefproject.model.ReefGraphqlArgument;
import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
public class GetRatePackageRequest implements ReefGraphqlArgument {

    @SerializedName("ratePackageId") private String ratePackageId;
}