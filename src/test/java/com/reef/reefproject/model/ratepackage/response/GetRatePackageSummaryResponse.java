package com.reef.reefproject.model.ratepackage.response;

import com.google.gson.annotations.SerializedName;
import com.reef.reefproject.model.ratesummary.RateSummaries;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Getter
@ToString
public class GetRatePackageSummaryResponse {

    @SerializedName("data")  private Data data;

    @Getter
    @ToString
    public static class Data{
        @SerializedName("getRatePackageSummary")
        private GetRatePackageSummary getRatePackageSummary;

        @Getter
        @ToString
        public static class GetRatePackageSummary{
            @SerializedName("type") private String type;
            @SerializedName("description") private String description;
            @SerializedName("activeDays") private String activeDays;
            @SerializedName("activeTimes") private  String activeTimes;
            @SerializedName("rateSummaries") private List<RateSummaries> rateSummaries;
        }

    }
}
