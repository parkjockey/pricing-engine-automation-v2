package com.reef.reefproject.model.ratepackage.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import com.reef.reefproject.model.fee.Fee;
import com.reef.reefproject.model.rateplan.RatePlan;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Getter
@ToString
public class RatePackageReefCloud {

    @SerializedName("ratePackageId") @JsonProperty("ratePackageId") private String id;
    @SerializedName("serviceCode") private String serviceCode;
    @SerializedName("marketName") private String marketName;
    @SerializedName("ratePackageName") @JsonProperty("ratePackageName") private String name;
    @SerializedName("currency") private String currency;
    @SerializedName("mapPrice") private String mapPrice;
    @SerializedName("taxInclusive") private Boolean taxInclusive;
    @SerializedName("ratePlans") private List<RatePlan> ratePlans;
    @SerializedName("fees") private List<Fee> fees;
    @SerializedName("shared") private Boolean shared;
    @SerializedName("bestRate") private Boolean bestRate;
    @SerializedName("realEstateId") private String realEstateId;
    @SerializedName("status") private String status;
    @SerializedName("createdAt") private String createdAt;
    @SerializedName("modifiedAt") private String modifiedAt;
    @SerializedName("longTerm") private Boolean longTerm;
}