package com.reef.reefproject.model.ratepackage.request;
import com.reef.reefproject.model.ReefGraphqlRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.File;

@Slf4j
public class GetRatePackageSummary extends ReefGraphqlRequest {

    @Getter @Setter private GetRatePackageSummaryRequest getRatePackageSummaryRequest;

    public GetRatePackageSummary(GetRatePackageSummaryRequest getRatePackageSummaryRequest){
        this.getRatePackageSummaryRequest = getRatePackageSummaryRequest;
    }

    @Override
    public String toString() {
        String graphqlFilePath = "src/test/resources/graphqlschema/getRatePackageSummary.graphql";
        File graphqlRequestModelFile = new File(graphqlFilePath);
        return super.stringify(graphqlRequestModelFile, getRatePackageSummaryRequest);
    }
}