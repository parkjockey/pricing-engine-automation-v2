package com.reef.reefproject.model.ratepackage.request;

import com.reef.reefproject.model.ReefGraphqlRequest;
import lombok.Getter;

import java.io.File;

public class CopyRatePackage extends ReefGraphqlRequest {

    @Getter private final CopyRatePackageRequest copyRatePackageRequest;

    public CopyRatePackage(CopyRatePackageRequest copyRatePackageRequest) {
        this.copyRatePackageRequest = copyRatePackageRequest;
    }

    @Override
    public String toString() {
        String graphqlFilePath = GRAPHQL_SCHEMA_FILE_PATH + "copyRatePackage.graphql";
        File graphqlRequestModelFile = new File(graphqlFilePath);
        return super.stringify(graphqlRequestModelFile, copyRatePackageRequest);
    }
}