package com.reef.reefproject.model.ratepackage.response;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class CreateRatePackageResponse {

    @SerializedName("data")
    private Data data;

    @Getter
    public static class Data {

        @SerializedName("createRatePackage")
        private RatePackageReefCloud createRatePackage;
    }
}