package com.reef.reefproject.model;

import com.google.gson.Gson;
import com.reef.reefproject.helpers.parsers.GraphqlParser;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;

@Slf4j
public abstract class ReefGraphqlRequest {

    protected static final String GRAPHQL_SCHEMA_FILE_PATH = "src/test/resources/graphqlschema/";

    protected String stringify(File graphqlModel, ReefGraphqlArgument instance) {
        Gson gson = new Gson();
        String parsedGraphqlPayload = "";
        try {
            parsedGraphqlPayload = GraphqlParser.getMappedGraphqlString(graphqlModel, gson.toJson(instance));
        } catch (IOException ex) {
            log.error("Failed to parse string from file: {}", graphqlModel);
            log.debug(ex.getMessage());
        }
        return parsedGraphqlPayload;
    }
}