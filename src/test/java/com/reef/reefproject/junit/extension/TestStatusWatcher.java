package com.reef.reefproject.junit.extension;

import com.reef.reefproject.junit.annotations.TestrailCaseId;
import com.reef.reefproject.helpers.annotationprocessor.JunitTagExtractor;
import com.reef.reefproject.helpers.annotationprocessor.TagExtractor;
import com.reef.reefproject.report.AllureManager;
import com.reef.reefproject.testrail.TestrailCaseStatus;
import com.reef.reefproject.testrail.TestrailHelpers;
import lombok.SneakyThrows;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;

import java.util.Locale;
import java.util.Optional;

import static com.reef.reefproject.constant.PropertyNameConstant.TESTRAIL;

public class TestStatusWatcher implements TestWatcher {

    private static final boolean IS_TESTRAIL_ENABLED = Boolean.parseBoolean(System.getProperty(TESTRAIL));

    @Override
    public void testDisabled(ExtensionContext context, Optional<String> reason) {
        TestWatcher.super.testDisabled(context, reason);
    }

    @Override
    public void testSuccessful(ExtensionContext context) {
        if (IS_TESTRAIL_ENABLED) {
            updateByExtractedTestrailAnnotationTagValue(context, TestrailCaseStatus.SUCCESS.getStatus());
        }
    }

    @Override
    public void testAborted(ExtensionContext context, Throwable cause) {
        TestWatcher.super.testAborted(context, cause);
    }

    @SneakyThrows
    @Override
    public void testFailed(ExtensionContext context, Throwable cause) {
        if (IS_TESTRAIL_ENABLED) {
            updateByExtractedTestrailAnnotationTagValue(context, TestrailCaseStatus.FAIL.getStatus());
        }

        // generate screenshot for ui tests
        boolean isContainUiTag = context.getTags().contains("ui");
        if (isContainUiTag) {
            String failingTestName = context.getDisplayName().replaceAll(" ", "_");
            AllureManager.takeScreenshotToAttachOnAllureReport(failingTestName);
        }
    }

    private static void updateByExtractedTestrailAnnotationTagValue(ExtensionContext context, Integer status) {
        TagExtractor extractor = new JunitTagExtractor(context);
        String testrailTagName = TestrailCaseId.class.getName().toLowerCase(Locale.ROOT);
        int caseId = extractor.extractByTagName(testrailTagName);
        // 0 is the default value returned for tests that lack the specified tag
        if (caseId != 0) {
            TestrailHelpers.addCaseToRun(caseId);
            TestrailHelpers.addResultWithStatusToCase(status, caseId);
        }
    }
}