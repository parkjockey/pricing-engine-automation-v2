package com.reef.reefproject.junit.extension;

import com.reef.reefproject.config.yamlbased.YamlConfigHandler;
import com.reef.reefproject.testrail.TestrailHelpers;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.platform.launcher.TestExecutionListener;
import org.junit.platform.launcher.TestPlan;

import java.util.Locale;

import static com.reef.reefproject.constant.PropertyNameConstant.TARGET_ENV;
import static com.reef.reefproject.constant.PropertyNameConstant.TESTRAIL;

@Slf4j
public class SingleExecutionSetup implements TestExecutionListener {

    @SneakyThrows
    @Override
    public void testPlanExecutionStarted(TestPlan testPlan) {
        // load environment-specific config from yaml
        String envName = System.getProperty(TARGET_ENV);
        log.info("Running test suite on: " + envName.toUpperCase(Locale.ROOT) + " environment");
        YamlConfigHandler.setConfigByEnvironmentName(envName);

        // testrail setup
        final boolean isTestrailEnabled = Boolean.parseBoolean(System.getProperty(TESTRAIL));
        if (isTestrailEnabled) {
            TestrailHelpers.setTestrailRunWithBaseName("Automated run: ");
        }
    }
}