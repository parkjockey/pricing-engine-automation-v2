package com.reef.reefproject.config.yamlbased;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.util.ArrayList;

@Getter
public class YamlConfigModel {
    @JsonProperty("environment-profiles") private ArrayList<Profile> profiles;

    @Getter
    public static class Profile {
        private String name;
        @JsonProperty("mw-module-url") private String mwModuleUrl;
        @JsonProperty("mw-gateway-url") private String mwGatewayUrl;
    }
}