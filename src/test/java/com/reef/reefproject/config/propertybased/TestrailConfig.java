package com.reef.reefproject.config.propertybased;

import org.aeonbits.owner.Config;

import static com.reef.reefproject.config.propertybased.OwnerConstant.VALUE_NOT_FOUND;

@Config.Sources({ "file:${user.dir}/src/test/resources/properties/testrail.properties" })
public interface TestrailConfig extends Config {

    @DefaultValue("Username " + VALUE_NOT_FOUND)
    @Key("username")
    String username();

    @DefaultValue("Password " + VALUE_NOT_FOUND)
    @Key("password")
    String password();

    @DefaultValue("Testrail project id " + VALUE_NOT_FOUND)
    @Key("your.project.testrail.project.id")
    int projectId();
}