package com.reef.reefproject.httpclient.graphql;

import com.reef.reefproject.config.yamlbased.YamlConfigHandler;
import com.reef.reefproject.model.ReefGraphqlRequest;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public abstract class ReefGraphqlClient {

    private static final String REEF_TARGET_URI = YamlConfigHandler.getEnvironmentConfig().getMwGatewayUrl();

    abstract public Response send(ReefGraphqlRequest request);

    protected static RequestSpecification getReefCommonReqSpec() {
        return new RequestSpecBuilder()
                .setBaseUri(REEF_TARGET_URI)
                .setContentType(ContentType.JSON)
                .addFilter(new GraphqlRestAssuredFilter())
                .setRelaxedHTTPSValidation()
                .build();
    }
}