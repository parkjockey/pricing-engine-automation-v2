package com.reef.reefproject.httpclient.graphql;

import com.reef.reefproject.config.propertybased.ConfigurationManager;
import com.reef.reefproject.model.ReefGraphqlRequest;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;

public class BasicAuthClient extends ReefGraphqlClient {

    private static final String USERNAME = ConfigurationManager.getAuthConfigInstance().username();
    private static final String PASSWORD = ConfigurationManager.getAuthConfigInstance().password();

    @Override
    public Response send(ReefGraphqlRequest request) {
        RequestSpecification rSpec = basicAuth().body(request.toString());

        return given()
                .spec(rSpec)
                .post()
                .then()
                .extract()
                .response();
    }

    private static RequestSpecification basicAuth() {
        return getReefCommonReqSpec().auth().preemptive().basic(USERNAME, PASSWORD);
    }
}
