package com.reef.reefproject.httpclient.graphql;

import com.reef.reefproject.config.propertybased.ConfigurationManager;
import com.reef.reefproject.model.ReefGraphqlRequest;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.util.Map;

import static io.restassured.RestAssured.given;

public class TokenAuthClient extends ReefGraphqlClient {

    private static final String ACCESS_TOKEN = ConfigurationManager.getAuthConfigInstance().accessToken();
    private static final Map<String, String> REEF_HEADERS = Map.of(
            "Organization", "1",
            "Authorization", ACCESS_TOKEN);

    @Override
    public Response send(ReefGraphqlRequest request) {
        RequestSpecification rSpec = tokenAuth().body(request.toString());

        return given()
                .spec(rSpec)
                .post()
                .then()
                .extract()
                .response();
    }

    private static RequestSpecification tokenAuth() {
        return getReefCommonReqSpec().headers(REEF_HEADERS);
    }
}
