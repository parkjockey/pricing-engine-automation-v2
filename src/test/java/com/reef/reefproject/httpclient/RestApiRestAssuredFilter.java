package com.reef.reefproject.httpclient;

import io.restassured.filter.Filter;
import io.restassured.filter.FilterContext;
import io.restassured.response.Response;
import io.restassured.specification.FilterableRequestSpecification;
import io.restassured.specification.FilterableResponseSpecification;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RestApiRestAssuredFilter implements Filter {

    @Override
    public Response filter(FilterableRequestSpecification requestSpec, FilterableResponseSpecification responseSpec, FilterContext ctx) {
        Response response = ctx.next(requestSpec, responseSpec);
        if (response.statusCode() != 200) {
            log.error("{} {} => \n Response headers => {} \n Response status => {} {} \n Response body => {}", requestSpec.getMethod(),
                    requestSpec.getURI(), response.getHeaders(), response.getStatusCode(), response.getStatusLine(), response.getBody());
        }
        log.debug("{} {} \n Request body => {} \n Response status => {} {} \n Response body => {}", requestSpec.getMethod(),
                requestSpec.getURI(), requestSpec.getBody(), response.getStatusCode(), response.getStatusLine(),
                response.getBody().prettyPrint());
        return response;
    }
}