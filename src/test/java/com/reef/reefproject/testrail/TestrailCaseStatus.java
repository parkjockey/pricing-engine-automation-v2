package com.reef.reefproject.testrail;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public enum TestrailCaseStatus {

    SUCCESS(1),
    FAIL(5);

    private final int status;
    private static final Map<Integer, TestrailCaseStatus> ENUM_MAP;

    TestrailCaseStatus(int status) {
        this.status = status;
    }

    public int getStatus() { return this.status; }

    static {
        Map<Integer, TestrailCaseStatus> map = new HashMap<>();
        for (TestrailCaseStatus instance : TestrailCaseStatus.values()) {
            map.put(instance.getStatus(), instance);
        }
        ENUM_MAP = Collections.unmodifiableMap(map);
    }

    public static TestrailCaseStatus get (int status) {
        return ENUM_MAP.get(status);
    }
}