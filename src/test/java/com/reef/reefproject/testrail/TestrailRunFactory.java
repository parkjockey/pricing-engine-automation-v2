package com.reef.reefproject.testrail;

import com.codepine.api.testrail.model.Run;
import com.reef.reefproject.config.propertybased.ConfigurationManager;
import com.reef.reefproject.helpers.generators.DatetimeGenerator;

public class TestrailRunFactory {

    private static final int RMS_TESTRAIL_SUITE_ID = ConfigurationManager.getTestrailConfigInstance().projectId();

    public static Run createForDefaultSuiteWithBaseName(String runNameBase) {
        String datetimeString = DatetimeGenerator.getDatetimeOfPattern("dd MMM yyy kk mm s");
        String runNameWithDatetime = runNameBase + " " + datetimeString;
        return new Run()
                .setSuiteId(RMS_TESTRAIL_SUITE_ID)
                .setName(runNameWithDatetime)
                .setIncludeAll(false);
    }
}
